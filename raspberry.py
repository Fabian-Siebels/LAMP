#!/usr/bin/python
# -*- coding: latin-1 -*-

import os
import time
import sys


def raspberry():
	while(True):
		os.system('cls' if os.name == 'nt' else 'clear')
		print("---Raspberry Pi Befehle---")
		print("1 = Raspberry Pi Version")
		print("2 = Hostname ändern")
		print("3 = Screen Session")
		option_in = int(raw_input(">"))
		if(option_in == 1):
			()
		if(option_in == 2):
			()
		if(option_in == 3):
			screen()


def screen():
	while(True):
		os.system('cls' if os.name == 'nt' else 'clear')
		print("---Screen Session Befehle---")
		print("1 = Installieren")
		print("2 = Tutorial")
		print("3 = Screen Sessions auflisten")
		option_in = int(raw_input(">"))
		if(option_in == 1):
			screeninstall()
		if(option_in == 2):
			screentut()
		if(option_in == 3):
			sessionlist()
            
def screeninstall():
    print("Screen wird installiert...")
    os.system("sudo apt-get install screen")
    
    
def screentut():
    print("-----------Screentutorial-----------")
    print("> screen bash    | Startet eine Session")
    print("> STRG + D       | Session beenden")
    print("> STRG + A und D | Session verlassen")
    print("> screen -list   | Auflistung der Sessions")
    print("> screen -r      | Session reconnect")

def sessionlist():
    print("Liste geladen...")
    os.system("screen -list")