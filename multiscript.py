#!/usr/bin/python
# -*- coding: latin-1 -*-

#LAMP Install by Fabian S.
import time
import sys
import os
import raspberry
import lampinstall

#Updatesektion
def beenden():
	print("Multihilfsscript by Fabian Siebels.")
	sys.exit(0)
	
def update():
	print("Updatevorgang ...")
	os.system("sudo apt-get update")
	
def upgrade():
	print("Upgradevorgang ...")
	os.system("sudo apt-get upgrade -y")
	

def oupdate():
	while(True):
		os.system('cls' if os.name == 'nt' else 'clear')
		print("----Update / Upgrade----")
		print("1 = Update")
		print("2 = Upgrade")
		print("3 = Back")
		option_in = int(raw_input(">"))
		if(option_in == 1):
			update()
		if(option_in == 2):
			upgrade()
		if(option_in == 3):
			insoption()
			
#Hauptsektion/Auswahlsektion		
while(True):
	os.system('cls' if os.name == 'nt' else 'clear')
	print("----LAMP Install by Fabian S.----")
	print("Akt. Zeit: "+ time.strftime("%d.%m.%Y %H:%M:%S"))
	print("----------Auswahl wählen----------")
	print("1 = Update/Upgrade")
	print("2 = LAMP Installationen")
	print("3 = Beenden")
	menu_in = int(raw_input(">"))
	if(menu_in == 1):
		oupdate()
	if(menu_in == 2):
		insoption()
	if(menu_in == 3):
		os.system('cls' if os.name == 'nt' else 'clear')
        beenden()