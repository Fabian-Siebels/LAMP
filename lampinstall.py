#!/usr/bin/python
# -*- coding: latin-1 -*-

#LAMP Install by Fabian S.
#Funktionen für das Mulitscript

import time
import sys
import os

def apache_inst():
	print("Apache Install ...")
	os.system("sudo apt-get install apache2 -y")
	
def apache_reboot():
	print("Apache reboot")
	os.system("sudo /etc/init.d/apache2 restart")
	print("Prüfen: http://<IP des Pis>")
    time.sleep(4)
			
def apache_i():
	while(True):
		os.system('cls' if os.name == 'nt' else 'clear')
		print("----Apache Install----")
		print("1 = Install")
		print("2 = Neustart erforderlich")
		print("3 = Back")
		option_in = int(raw_input(">"))
		if(option_in == 1):
			apache_inst()
		if(option_in == 2):
			apache_reboot()
		if(option_in == 3):
			insoption()
			

def php_ins():
	print("PHP installation ...")
	os.system("sudo apt-get install php7.0 php7.0-cli php7.0-curl php7.0-gd php7.0-intl php7.0-json php7.0-mbstring php7.0-mcrypt php7.0-mysql php7.0-opcache php7.0-readline php7.0-xml php7.0-xsl php7.0-zip php7.0-bz2 libapache2-mod-php7.0 -y")
	
def php_test():
	os.system("sudo mv test.php /var/www/html/")
	
def php_i():
	while(True):
		os.system('cls' if os.name == 'nt' else 'clear')
		print("----PHP Install + TEST----")
		print("1 = Installieren")
		print("2 = Testen")
		print("3 = Back")
		option_in = int(raw_input(">"))
		if(option_in == 1):
			php_ins()
		if(option_in == 2):
			php_test()
		if(option_in == 3):
			insoption()

def mysqllesen():
	os.system('cls' if os.name == 'nt' else 'clear')
	print("Installation von MySQL")
	print("Auf Installieren drücken")
	print("Nach einer Zeit kommt ein Dialogfenster,")
	print("wo man sein Passwort für den root eingibt.")
	print("Im zweiten Dialog gibt man es nochmal ein.")
	print("Geht in 5 Sek weiter")
	time.sleep(6)

def mysql_ins():
	print("MySQL installieren ...")
	os.system("sudo apt-get install mysql-server mysql-client -y")
			
def mysql_i():
	while(True):
		os.system('cls' if os.name == 'nt' else 'clear')
		print("----MySQL Installieren----")
		print("1 = Zuerst lesen")
		print("2 = Installieren")
		print("3 = Back")
		option_in = int(raw_input(">"))
		if(option_in == 1):
			mysqllesen()
		if(option_in == 2):
			mysql_ins()
		if(option_in == 3):
			insoption()
	

def insoption():
	while(True):
		os.system('cls' if os.name == 'nt' else 'clear')
		print("---- Installations Optionen----")
		print("1 = Apache 2")
		print("2 = PHP")
		print("3 = MySQL")
		print("4 = phpMyAdmin")
		print("5 = Beenden")
		option_in = int(raw_input(">"))
		if(option_in == 1):
			apache_i()
		if(option_in == 2):
			php_i()
		if(option_in == 3):
			mysql_i()
		if(option_in == 4):
 			phpmyadmin_i()
		if(option_in == 5):
			os.system('cls' if os.name == 'nt' else 'clear')
			beenden()